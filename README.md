# Getting Started with the Meso@LR cluster

## About
This is a training tutorial to lean how to use the cluster Meso@LR. 


## Useful links

* [Meso@LR](https://meso-lr.umontpellier.fr/) - Landing page for getting started with Meso@LR cluster.    
* [CheatSheet Slurm](https://nextcloud.inrae.fr/s/qs97RCJGF5tkMeK) - TLDR for slurm commands.  
* [Meso@LR user documentation](https://meso-lr.umontpellier.fr/documentation-utilisateurs/) - Documentation made by the admins of Meso@LR  
* [Meso@LR admin mail](mailto:meso-tech-calcul@umontpellier.fr) - Where to ask if you need help. 
* [CNRS HPC related mailing-list](https://calcul.math.cnrs.fr/) - Where to ask if you need MORE help. Big mailing list of developpers, engineers and everything centered about HPC. 

## How to open an acount on Meso@LR
Return both forms filled and signed to [Meso@LR Mail](mailto:isdm-calcul@umontpellier.fr)
1. [Application form](https://meso-lr.umontpellier.fr/wp-content/uploads/2021/03/1-Meso_dde_Calcul_nomprojets.doc)
2. [IT Charter](https://meso-lr.umontpellier.fr/wp-content/uploads/2021/03/20210319-MESO-CALCUL-CharteInformatique.pdf)

# How this training works

You should first clone the repository when you login to Meso@LR by executing this command =>
```bash
git clone https://forgemia.inra.fr/formationcalcul2023/formation-slurm.git
```

Cloning the repo will just copy all the contents you see on your screen to folders on Meso@LR. If you don't know how to connect to Meso@LR, please do [Exercise 1](./Exercise1-Connect_to_MesoLR) first.

Multiple folders are available each one containing a different exercice. You can do all of them or you can pick the ones you think you need the most.

Let's compute!

**IMPORTANT**: *You should run your jobs out of the folder named "scratch" on your account (which is a link to /lustre/$USER). The filesystem is a very fast one and provide vast amounts of storage. Do not run jobs out of your home, mainly because it won't work (when running a job on the cluster, everything expect "scratch" is mounted as read-only). These filesystems are slow and should only be used for backing-up the files that you produce on "scratch". Your /home directory on the cluster should only be used for storing source code, executables, Conda environments, singularity images and small data sets*.

# Glossary
``` bash
jobs             # A job is a program launched via SLURM.
nodes            # A compute server
login-nodes      # A server used to login on Meso@LR
```

# Additionnal ressources

* [HPC tutorial of New Mexico State University](https://hpc.nmsu.edu/discovery/)
* [HPC tutorial of Bern](https://hpc-unibe-ch.github.io/)

# Authors
[Jérémy Verrier](mailto:jeremy.verrier@inrae.fr)

HPC engineer at [INRAE](https://www.inrae.fr/)



# Other contributors
HPC support team at [INRAE](https://www.inrae.fr/)
* [Alexandre Dehne Garcia](mailto:alexandre.dehne-garcia@inrae.fr)
* [Adrien Falce](mailto:adrien.falce@inrae.fr)
* [Jacques Lagnel](mailto:jacques.lagnel@inrae.fr)
* [Eric Maldonado](mailto:eric.maldonado@inrae.fr)
* [Tovo Rabemanant](mailto:tovo.rabemanantsoa@inrae.fr)

# Special thanks to 
Forgemia team
