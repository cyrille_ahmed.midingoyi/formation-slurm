#! /usr/bin/env bash

# PARAMETERS GOES HERE

#SBATCH -A inrae-infra                                # GROUP
#SBATCH -p inrae                                      # PARTITION

#SBATCH --output=/lustre/%u/logs/first-job-%j.out     # Output file
#SBATCH --error=/lustre/%u/logs/first-job-%j.err      # Error file

# MY COMMAND HERE =>
hostname                                              # Show hostname of compute node
sleep 60                                              # Wait 1 minute
echo "I am done waiting!"
