# Usefull commands

Usefull commands that can be used in Meso@LR.

* BASH
* SLURM
* Singularity
* R


## BASH
Usefull tool to check your scipt syntaxes and best practices: [Shell check](https://www.shellcheck.net/)

Google Shell best practices: https://google.github.io/styleguide/shellguide.html 
```bash
# Linux and Windows does not have the same line braks
# If you upload a windows file and encounter this =>
# syntax error: `^M’ unexpected
# Launch this command on the incriminated file
dos2unix windows.file

# Printing space available on the server
df -h

# Printing space used by a specific folder
du -sh FOLDER

# Find file older than 60 days
find FOLDER -mtime +60 -print

# Get nth (where nth is a number) line of a file
sed -i ${nth}p file.txt

# Move all files in a directory into a subdirectory
tree
.
|-- 1f77068d8d
|-- 2288366b3c
|-- 5338790bdf
|-- a645ee91b3
|-- aff3a4b562
|-- b4488235b5
|-- db1e60bf66
|-- dc32f5331f
|-- f5bf3ca9f5
|-- fb76033f97
`-- inputs
mv./* inputs
tree
.
`-- inputs
    |-- 1f77068d8d
    |-- 2288366b3c
    |-- 5338790bdf
    |-- a645ee91b3
    |-- aff3a4b562
    |-- b4488235b5
    |-- db1e60bf66
    |-- dc32f5331f
    |-- f5bf3ca9f5
    `-- fb76033f97

# Modify return case to space in a file
tr '/n' ' ' file.txt

# Print nth (where nth is a number) column in a file separated by ","
cut -d"," -f${nth}

# Get number of lines in a file
wc -l file.txt

# Know your public ip
curl icanhazip.com

# Extract a zip file
unzip file.zip

# Extract a tar.gz file
tar xvzf file.tar.gz

# Extract a tar.bz2 file
tar xvf file.tar.bz2

# Extract a tar file
tar xvf file.tar

# Loop on a list
for element in "first second third"; do
  echo $element
done

# if $var empty
if [[ -z $var ]]; then
  echo "var is empty"
else
  echo "var is not empty"
fi
# Download a file on the internet
wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.3.0-amd64-netinst.iso
```
## SLURM

MAXVMSIZE MAX RAM used by a job (doesn't work on Meso@LR)
```
sacct --format JobID,jobname,MaxVMSize -j JOBID
```

If SLURM error is getting weird =>
```
sjobexitmod -l JOBID
```
Where: 
```
    0 → success 
    non-zero → failure 
    Exit code 1 indicates a general failure 
    Exit code 2 indicates incorrect use of shell builtins 
    Exit codes 3-124 indicate some error in job (check software exit codes) 
    Exit code 125 indicates out of memory 
    Exit code 126 indicates command cannot execute 
    Exit code 127 indicates command not found 
    Exit code 128 indicates invalid argument to exit 
    Exit codes 129-192 indicate jobs terminated by Linux signals 
        For these, subtract 128 from the number and match to signal code 
        Enter kill -l to list signal codes 
        Enter man signal for more information
```

## Singularity 


## R
### List packages installed
```R
my_packages <- as.data.frame(installed.packages()[ , c(1, 3:4)])
my_packages <- my_packages[is.na(my_packages$Priority), 1:2, drop = FALSE]  
rownames(my_packages) <- NULL 
my_packages 
```

https://rviews.rstudio.com/2017/03/29/r-and-singularity/

### Args and R =>

```R
AA <- print(commandArgs(trailingOnly=TRUE)) 
AA[2] 
Rscript test.R 2 3
3
```

### Install package list
```R
packages <- c("climatrends","e1071","fitdistrplus","hydroGOF","hydroTSM","maptools","parallel","plotrix","raster","zoo","DDM","hydrostats","lfstat")

# Install packages not yet installed 
installed_packages <- packages %in% rownames(installed.packages()) 
if (any(installed_packages == FALSE)) { 
  install.packages(packages[!installed_packages]) 
}
```
