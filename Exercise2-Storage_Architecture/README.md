# Storage architecture

Start by launching the following commands and analysing the results
```bash
# pwd is a command that tells you where you are
pwd
# ls stand for list, and $USER is your login name
ls -l /home/$USER

ls -l 
ls -l scratch
# The "*" is a wildcard. It can be any number of values or none after "work_"
# When you are not certain about a folder name, this can be usefull
ls -l work_*

# df is a command to show you disk space available, used and more
df -h
```

## Permanent storage

### **/nfs/home** =>
When you login, you spawn in /home/$USER which is a link  to /nfs/home/$USER. <br />
This space should be used for storing source code, executables, Conda environments, singularity images and small data sets.
```bash
[verrierj@muse-login01 ~]$ ls -l /home
lrwxrwxrwx 1 root root 9 Nov 15  2017 /home -> /nfs/home
```

### **/nfs/work** =>
You can have one or multiple work folders. They are the ones named "work_X" in your HOME. It is a link to the folder /nfs/work/work_X
```bash 
[verrierj@muse-login01 ~]$ ls -l work_inrae-infra
lrwxrwxrwx 1 root root 21 Feb 15 14:30 work_inrae-infra -> /nfs/work/inrae-infra
```
They are mainly used to share data inputs, codes, conda environments and singularity image to other people in the same group as you.

### **/storage** =>
Resilient storage that is snapshoted and can be replicated. It can also be mounted to the cluster.

15 PetaBytes are available and it can be used to store data for years to come.

If you want to access it, you have to fill and sign =>
* [Storage form](https://meso-lr.umontpellier.fr/wp-content/uploads/2021/03/1-Meso_dde_Data_nomprojets.xlsx)
* [IT charter](https://meso-lr.umontpellier.fr/wp-content/uploads/2021/03/20210312_CharteMeso-Data.pdf)

## Computing storage
### **/lustre** =>
 The folder named "scratch" in your HOME directory is a link to /lustre/$USER. You should run your jobs out of this folder. The filesystem is a very fast one and provide vast amounts of storage. Do not run jobs out of your home, mainly because it won't work **when running a job on the cluster, everything expect "scratch" is mounted as read-only**.
```bash
[verrierj@muse-login01 ~]$ ls -l scratch
lrwxrwxrwx 1 verrierj irstea 16 Mar  8 07:26 scratch -> /lustre/verrierj
```

**IMPORTANT**: *You should run your jobs out of the folder named "scratch" on your account (which is a link to /lustre/$USER). The filesystem is a very fast one and provide vast amounts of storage. Do not run jobs out of your home, mainly because it won't work (when running a job on the cluster, everything expect "scratch" is mounted as read-only). These filesystems are slow and should only be used for backing-up the files that you produce on "scratch". Your /home directory on the cluster should only be used for storing source code, executables, Conda environments, singularity images and small data sets*.

# Exercise
Everything that you will do will probably start by uploading the inputs file of your code to the cluster Meso@LR.

The goal here is to know how to do just that.

Transfer a random file to the Meso@LR cluster by using either MobaXterm embedded sftp tool (left panel) or any data exchange command/tool of your choice (scp, rsync, sftp, filezilla, WinSCP ...)

Using scp =>
```bash
jverrier@MOP1713 MINGW64 ~/Desktop/formation-slurm (main)
$ scp README.md verrierj@muse-login.meso.umontpellier.fr:.
verrierj@muse-login.meso.umontpellier.fr's password: 
READMEmd         100% 2112    44.3KB/s   00:00    
```

MobaXterm =>
![MobaXterm Upload](./mobaxterm-upload.png?raw)

* By default, what is the default folder of the file you just uploaded?
* If the file needed to be rewritten by your code, where should we put it?