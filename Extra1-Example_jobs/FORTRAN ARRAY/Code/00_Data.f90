MODULE Data_TD

    IMPLICIT NONE

    CHARACTER(LEN=256), PARAMETER :: sFileParam = './param.txt'   !< Nom du fichier param

    ! définition du type de structure DataObs contenant les données observées
    TYPE DataObs
        INTEGER, DIMENSION(4) :: tDate                          !< Date de la donnée
        REAL :: tQ                                              !< Débits observés (en L/s)
        REAL :: tQm3s                                           !< Débits observés (en m3/s)
        REAL :: tQmmj                                           !< Débits observés (en mm/j)
        REAL :: tQclass                                         !< Débits classés (en L/s)
        REAL :: tP                                              !< Pluies observées (en mm)
        REAL :: tETP_O                                          !< ETP Oudin observées (en mm)
    END TYPE

    ! définition d'une structure de type DataObs
    TYPE(DataObs), DIMENSION(:), ALLOCATABLE :: tDataObs

END MODULE
