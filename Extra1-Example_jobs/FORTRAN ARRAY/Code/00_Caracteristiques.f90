MODULE Caracteristiques
    ! Chargement des éléments du module Data_TD
    USE Data_TD, ONLY : tDataObs

    IMPLICIT NONE

    ! Déclarations
    REAL :: Qmoyen                                 !< Débit moyen (en L/s)
    REAL, DIMENSION(11) :: tQQuant                 !< Quantiles de débits (en L/s)
    REAL :: Pmoy, ETPmoy, Qmmjmoy                  !< Pluie, ETP et débit moyens interannuels (en mm)
    REAL, DIMENSION(:), ALLOCATABLE :: Pan         !< tableau des cumuls annuels
    REAL, DIMENSION(:), ALLOCATABLE :: ETPan       !< tableau des cumuls annuels
    REAL, DIMENSION(:), ALLOCATABLE :: Qan         !< tableau des cumuls annuels
    REAL, DIMENSION(:), ALLOCATABLE :: QanSim      !< tableau des cumuls simulés


    CONTAINS

    ! Calcul des caractéristiques du BV
    SUBROUTINE Caract_BV
        IMPLICIT NONE

        INTEGER :: iBou                            !< Incrément de boucle

        ! Calcul du débit moyen (méthode simple)
        Qmoyen = Qmoy(tDataObs%tQm3s)

        ! Classement des débits
        CALL Calc_Qclass(tDataObs(:)%tQm3s, tDataObs(:)%tQclass)

        ! Calcul des quantiles de débits
        DO iBou = 0, 100, 10
            tQQuant(iBou/10 + 1) = Calc_Quant(tDataObs(:)%tQclass, iBou)
        END DO

        ! Calcul annee hydrologique
        CALL Annee_Hydro(tDataObs(:)%tDate(1),tDataObs(:)%tDate(2),tDataObs(:)%tDate(4))

        ! Calcul du débit moyen interannuel
        CALL CalcMoyAn (tDataObs(:)%tQmmj, tDataObs(:)%tDate(4), Qmmjmoy, Qan)

        ! Calcul de la pluie moyenne interannuelle
        CALL CalcMoyAn (tDataObs(:)%tP, tDataObs(:)%tDate(4), Pmoy, Pan)

        ! Calcul de l'ETP moyenne interannuelle
        CALL CalcMoyAn (tDataObs(:)%tETP_O, tDataObs(:)%tDate(4), ETPmoy, ETPan)


    END SUBROUTINE

    ! Fonction de calcul du débit moyen (simple)
    REAL FUNCTION Qmoy(Q)

        IMPLICIT NONE

        !Déclarations
        REAL, DIMENSION(:), INTENT(IN) :: Q    !< Débits observés
        INTEGER :: iBou                        !< Incrément de boucle
        INTEGER :: n                           !< Taille du vecteur des débits
        INTEGER :: iSize                       !< Compteur des données non négatives
        REAL :: Qsum                           !< Somme des débits non négatifs

        ! Initialisations
        iSize = 0
        Qsum = 0
        n = SIZE(Q)

        ! Nombre de débits non nuls
        DO iBou = 1, n
            IF (Q(iBou) .LT. 0) CYCLE
            IF (Q(iBou) .GE. 0) THEN
                Qsum = Qsum + Q(iBou)
                iSize = iSize + 1
            END IF
        END DO

        ! Calcul du débit moyen
        Qmoy = Qsum / FLOAT(iSize)

    END FUNCTION

    ! Classement des débits
    SUBROUTINE Calc_Qclass (Q, Qclass)

        IMPLICIT NONE

        ! Déclarations
        REAL, DIMENSION(:), INTENT(IN) :: Q         !< Débits observés
        REAL, DIMENSION(:), INTENT(OUT) :: Qclass   !< Débits classés
        INTEGER :: iBou                             !< Incrément de boucle
        INTEGER :: K                                !< Rang de classement au sein  de X

        ! Initialisation
        Qclass(:) = 99999999.

        ! Classement des debits
        DO iBou = 1, SIZE(Q)
            CALL CLASC1(iBou, Qclass, Q(iBou), K)
        END DO

    END SUBROUTINE

    ! **********************************************************************
    ! Classe une donnée X1 dans un vecteur X dans l'ordre croissant
    SUBROUTINE CLASC1(N,X,X1,K)
    ! X1 : Valeur à classer
    ! N : nombre total de valeurs déjà classées
    ! K : Rang de classement au sein  de X
    ! X : Vecteur dans lequel on classe X1
    ! **********************************************************************
        IMPLICIT NONE

        INTEGER, INTENT(IN) :: N                  ! N : nombre total de valeurs déjà classées
        REAL, DIMENSION(*), INTENT(INOUT) :: X    ! X : Vecteur dans lequel on classe X1
        REAL, INTENT(IN) :: X1                    ! X1 : Valeur à classer
        INTEGER, INTENT(OUT) :: K                 ! K : Rang de classement au sein  de X

        INTEGER :: I, J ! Entiers de boucles

        K = N + 1

        IF(X1.GE.X(N)) RETURN ! Cas où la valeur à classer est plus grande que la limite du tableau (valeur d'initialisation), alors non classé

        DO I = 1, N ! on cherche le rang au sein des valeurs déjà classées où insérer la nouvelle valeur

            IF(x1.LT.X(I)) THEN
                K = I ! rang de classement
                IF(I.LT.N)THEN ! on identifie le rang au sein de la série, et on décale toute la partie supérieure du tableau pour laisser une place au sein du tableau où insérer la nouvelle valeur
                    DO J = N, I+1, -1
                        X(J) = X(J-1)
                    ENDDO
                ENDIF

                X(I) = X1 ! on place la nouvelle valeur au sein du tableau

                RETURN
            ENDIF
        ENDDO
    END SUBROUTINE CLASC1

    ! Fonction de calcul des quantiles de débits
    REAL FUNCTION Calc_Quant(Qclass, n)

        IMPLICIT NONE

        ! Déclarations
        REAL, DIMENSION(:), INTENT(IN) :: Qclass    !< Tableau des débits classés
        INTEGER, INTENT(IN) :: n                    !< Quantile (en %)

        REAL, DIMENSION(:), ALLOCATABLE :: QclassClean !< Tableau des débits classés nettoyés (sans les valeurs négatives)
        INTEGER :: iBou                              !< Incrément de boucle

        ! Recherche des débits à ignorer
        DO iBou = 1, SIZE(Qclass)
            IF (Qclass(iBou) .GE. 0) EXIT
        END DO

        ! Allocation du tableau de débits classés nettoyés
        IF(ALLOCATED(QclassClean)) DEALLOCATE(QclassClean)
        ALLOCATE(QclassClean(SIZE(Qclass) - iBou + 1))

        ! Remplissage du tableau QclassClean
        QclassClean = Qclass(iBou:SIZE(Qclass))

        ! Calcul du quantile n
        Calc_Quant = -99
        Calc_Quant = QclassClean(SIZE(QclassClean) * n/100)

    END FUNCTION

    ! Calcul de la moyenne interannuelle
    SUBROUTINE CalcMoyAn(tData, tAnn, tDataMoy, tCumAnn)

        IMPLICIT NONE

        ! Déclarations
        REAL, DIMENSION(:), INTENT(IN) :: tData        !< Vecteur des données en entrée
        INTEGER, DIMENSION(:), INTENT(IN) :: tAnn      !< Vecteur des années des données en entrée
        REAL, INTENT(OUT) :: tDataMoy                  !< Moyenne interannuelle

        INTEGER :: iBou                                !< incrément de boucle
        INTEGER :: iAn                                 !< Année en cours
        INTEGER :: n                                   !< taille du vecteur de données en entrée
        INTEGER :: iSize                               !< compteur des données non négatives
        REAL :: tDataSum                               !< somme des données non négatives
        REAL, DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: tCumAnn     !< tableau des cumuls annuels
        INTEGER :: iCum                                !< indice de l'année en cours

        ! Initialisations
        iSize = 0
        iCum = 1
        iAn = tAnn(1)
        tDataSum = 0
        n = SIZE(tData)

        ! Allocation du tableau tCumAnn
        IF (ALLOCATED(tCumAnn)) DEALLOCATE(tCumAnn)
        ALLOCATE(tCumAnn(tAnn(n) - iAn + 1))

        tCumAnn(:) = 0

        ! Boucle pour déterminer le nombre de données et la somme des valeurs non négatives dans une année
        DO iBou = 1, n
            IF (tAnn(iBou) .NE. iAn) THEN
                IF(iSize .LT. 300) tCumAnn(iCum) = -99
                iCum = iCum +1
                iSize = 1
                iAn = tAnn(iBou)
            ENDIF
            IF (tData(iBou) .LT. 0) CYCLE
            tCumAnn(iCum) = tCumAnn(iCum) + tData(iBou)
            iSize = iSize + 1
        END DO

        ! Contrôle du nombre de données pour la dernière année
        IF(iSize .LT. 300) THEN
            tCumAnn(iCum) = -99
        END IF

        ! Initialisations
        iSize = 0
        tDataSum = 0

        ! Calcul de la moyenne interannuelle
        DO iBou = 1, iCum
            IF(tCumAnn(iBou) .LT. 0) CYCLE
            tDataSum = tDataSum + tCumAnn(iBou)
            iSize = iSize + 1
        END DO

        IF(iSize .NE. 0) tDataMoy = tDataSum / FLOAT(iSize)

    END SUBROUTINE

    SUBROUTINE Annee_Hydro(AnC,Mois,AnH)

        IMPLICIT NONE

        INTEGER, DIMENSION(:), INTENT(IN) :: AnC      !< Vecteur des années des données en entrée
        INTEGER, DIMENSION(:), INTENT(IN) :: Mois     !< Vecteur des années des données en entrée
        INTEGER, DIMENSION(:), INTENT(OUT) :: AnH     !< Vecteur des années des données en entrée
        INTEGER :: DebMois, i              !< incrément de boucle

        DebMois=10
        AnH = 0
        DO i = 1,SIZE(AnC)
            IF (Mois(i).GE.DebMois) THEN
                AnH(i) = AnC(i)
            ELSE
                AnH(i) = AnC(i)-1
            ENDIF
        END DO

    END SUBROUTINE

END MODULE
