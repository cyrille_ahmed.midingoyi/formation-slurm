MODULE Lecture

    ! Chargement du module Data_TD
    USE Data_TD

    IMPLICIT NONE

    ! Déclarations pour tout le module
    CHARACTER (LEN = 256) :: sChemin               !< Chemin où se situent les données
    CHARACTER (LEN = 8) :: sBV                     !< Code bassin à lire dans le fichier param.txt
    CHARACTER (LEN = 256) :: sCheminRes            !< Chemin du dossier résultats
    REAL :: fSurf                                  !< Surface du bassin (en km2)


    CONTAINS

    SUBROUTINE LecConfig
        IMPLICIT NONE

        ! Déclaration des variables
!        CHARACTER (LEN = 256), INTENT(OUT) :: sChemin              !< Chemin à lire dans le fichier param.txt (pas nécessaire si définis dans le module)
!        CHARACTER (LEN = 8), INTENT(OUT) :: sBV                    !< Code bassin à lire dans le fichier param.txt (pas nécessaire si définis dans le module)
        CHARACTER(LEN=10) :: pname                                  !< Nom du paramètre
        CHARACTER(LEN=256) :: pvalue                                !< Valeur du paramètre
        CHARACTER(LEN=10) :: arg                                    !< Argument de la ligne de commande qui a lance le programme
        INTEGER :: iLu                                              !< Numéro alloué au fichier
        INTEGER :: iErr                                             !< Entier de test du format lu
        INTEGER :: iBou                                             !< Incrément de boucle

        LOGICAL :: bOk                                              !< Booléen pour existance du fichier

        ! Initialisations
        iLu = 0
        sBV=''
        sChemin='./Inputs/'
        sCheminRes='./Outputs/'

        ! Ouverture du fichier param.txt
        ! Contrôle de l'existance du fichier
        INQUIRE(FILE=sFileParam,EXIST=bOk)
        IF(bOk) THEN
            ! Ouverture du fichier
            OPEN(iLu, file = sFileParam, status = 'old')
        ELSE
            ! Fin du programme si le fichier n'existe pas
            STOP "Fichier parametre introuvable"
        ENDIF

        ! Passage des lignes d'entête
        DO iBou = 1, 2
            READ(iLu, *)
        END DO

        ! Récupération des données du fichier param.txt
        WRITE(*,'(A)') '## Lecture du fichier param.txt ##'
        DO
            READ(iLu, '(A10,A256)', IOSTAT = iErr) pname, pvalue

            IF(iErr.LT.0) EXIT ! Fin du fichier

            ! Options sur le nom du paramètre
            SELECT CASE(TRIM(pname))
                CASE('BV')
                    WRITE(*,'(2A)') 'BV = ', TRIM(pvalue)
                    sBV = TRIM(pvalue)
                CASE('PATH_DATA')
                    WRITE(*,'(2A)') 'PATH_DATA = ', TRIM(pvalue)
                    sChemin = TRIM(pvalue)
                CASE('PATH_RES')
                    WRITE(*,'(2A)') 'PATH_RES = ', TRIM(pvalue)
                    sCheminRes = TRIM(pvalue)
            END SELECT
        END DO

        ! Gestion absence de valeurs dans fichier param.txt
        IF(TRIM(sBV).EQ.'') THEN
            CALL getarg(1,arg) ! Recupere le premier argument de la ligne de commande qui a lance le programme
            WRITE(*,'(2A)') 'BV = ', TRIM(arg)
            sBV = TRIM(arg)
        END IF

        ! Fermeture du fichier param.txt
        CLOSE(iLu)

    END SUBROUTINE LecConfig

    SUBROUTINE LecData
        IMPLICIT NONE
!        CHARACTER (LEN = 256), INTENT(IN) :: sChemin               !< Chemin à lire dans le fichier param.txt (pas nécessaire si définis dans le module)
!        CHARACTER (LEN = 8), INTENT(IN) :: sBV                     !< Code bassin à lire dans le fichier param.txt (pas nécessaire si définis dans le module)
        CHARACTER (LEN = 256) :: sFic                               !< Chemin du fichier de bassin

        INTEGER :: iLu                                              !< Numéro alloué au fichier
        INTEGER :: k, iBou                                          !< Incrément de boucle
        INTEGER :: iSize                                            !< Taille du tableau à définir
        INTEGER :: iErr                                             !< Entier de test du format lu

        ! Initialisations
        iLu = 0
        iSize = 0

        ! Initialisation du chemin du fichier de BV
        sFic = TRIM(sChemin)//TRIM(sBV)//"_BV.txt"

        ! Ouverture du fichier de bassin
        OPEN(iLu, FILE = TRIM(sFic), STATUS = "OLD")

        ! Boucle pour sauter les lignes d'entête
        DO iBou = 1, 24
            READ(iLu, *)
        END DO

        !READ(iLu, '(23X,F12.0)') fSurf
        READ(iLu, '(36X,F12.0)') fSurf

        DO iBou = 26, 51
            READ(iLu, *)
        END DO

        ! Passage du fichier pour déterminer le nombre de lignes
        DO
            READ(iLu, *, IOSTAT = iErr)
            IF(iErr .LT. 0) EXIT
            iSize = iSize + 1
        END DO

        ! Allocation de la structure de données observées
        IF (ALLOCATED(tDataObs)) DEALLOCATE(tDataObs)
        ALLOCATE(tDataObs(iSize))

        tDataObs(:)%tQm3s = -99
        tDataObs(:)%tQmmj = -99

        ! Retour au début du fichier
        REWIND(iLu)

        ! Boucle pour sauter les lignes d'entête
        DO iBou = 1, 51
            READ(iLu, *)
        END DO

        ! Lecture des 10 premières lignes
        DO iBou = 1, iSize
            READ(iLu, '(I4, I2, I2, 1X,F8.0,15X,F5.1,13X,F5.1)', IOSTAT = iErr) (tDataObs(iBou)%tDate(k), k=1,3),  &
                tDataObs(iBou)%tQ, tDataObs(iBou)%tP, tDataObs(iBou)%tETP_O

            IF(iErr.LT.0) EXIT ! On a atteint la fin du fichier

            ! Conversion en m3/s
            IF(tDataObs(iBou)%tQ .GE. 0.) tDataObs(iBou)%tQm3s = tDataObs(iBou)%tQ/1000.

            ! Conversion en mm/j
            IF(tDataObs(iBou)%tQ .GE. 0.) tDataObs(iBou)%tQmmj = tDataObs(iBou)%tQ * 36. * 24. / (10000. * fSurf)

!            write(*, '("Le ",I2.2,"/",I2.2,"/",I4,", le debit vaut", F8.2, " (m3/s),&
!                & la pluie vaut", F5.1, " (mm) et l''ETP vaut", F5.1, " (mm)")') &
!                tDataObs(iBou)%tDate(3),tDataObs(iBou)%tDate(2),tDataObs(iBou)%tDate(1), &
!                tDataObs(iBou)%tQm3s, tDataObs(iBou)%tP, tDataObs(iBou)%tETP_O
        END DO

        ! Fermeture du fichier de bassin
        CLOSE(iLu)

    END SUBROUTINE LecData

END MODULE
