#!/bin/bash

module load gcc/5.2.0

###########
## INPUT ##
###########
PATH_SRC='/nfs/home/alban.de-lavenne/scratch/Exemple_FORTRAN/Code/'
PATH_PRG='/nfs/home/alban.de-lavenne/scratch/Exemple_FORTRAN/'
PRG='GR1A'

#################
## COMPILATION ##
#################
cd $PATH_SRC
FORFILES=(00_Data.f90 00_Caracteristiques.f90 00_Lecture.f90 00_Ecriture.f90 00_Modele.f90 main.f90)
gfortran -O2 -w -o $PRG ${FORFILES[*]}
mv $PRG $PATH_PRG

###############
## EXECUTION ##
###############
#BVLIST=('Y6234010' 'Y6224020' 'Y6204020' 'U2345030' 'U2345020' 'H7713010' 'H7702010' 'A0220200')
#cd $PATH_PRG
#for BV in ${BVLIST[*]}
#do
#./$PRG $BV
#done
