MODULE Ecriture
    ! Chargement des modules
    USE Caracteristiques
    USE Data_TD
    USE Lecture

    CONTAINS

    SUBROUTINE EcritData(sFic)

        IMPLICIT NONE

        ! Déclarations
        CHARACTER(LEN=256), INTENT(IN) :: sFic
        INTEGER :: iLu

        iLu = 0
        write(*,*) sFic

        ! Ouverture du fichier
        OPEN(iLu, file = sFic, status = 'unknown')

        ! Eciture d'une entête des lignes d'entête
        WRITE(iLu, '(A)') "----------------------------------------------------"
        WRITE(iLu, '(A)') " Caractéristiques du bassin "//TRIM(sBV)
        WRITE(iLu, '(A)') "----------------------------------------------------"

        ! Ecriture des caractéristiques
        WRITE(iLu, '(A)') ""
        WRITE(iLu, '("Débit moyen (m3/s) :", F5.1)') Qmoyen
        WRITE(iLu, '("Débit minimal (m3/s) :", F5.1)') tQQuant(1)
        WRITE(iLu, '("Débit maximal (m3/s) :", F5.1)') tQQuant(SIZE(tQQuant))
        WRITE(iLu, '("Quantiles 10 à 90 (m3/s) :", 9(F5.1, ";"))') tQQuant(2:10)
        WRITE(iLu, '("Pluie moyenne interannuelle (mm) :", F7.1)') Pmoy
        WRITE(iLu, '("ETP moyenne interannuelle (mm) :", F6.1)') ETPmoy
        WRITE(iLu, '("Q moyen interannuel (mm) :", F6.1)') Qmmjmoy

        ! Fermeture du fichier
        CLOSE(iLu)
    END SUBROUTINE

    SUBROUTINE EcritSim(sFic)

        IMPLICIT NONE

        ! Déclarations
        CHARACTER(LEN=256), INTENT(IN) :: sFic
        INTEGER :: i,iLu

        iLu = 0
        write(*,*) sFic

        ! Ouverture du fichier
        OPEN(iLu, file = sFic, status = 'unknown')

        ! Ecriture des simulations
        WRITE(iLu,'(5A8)') "AN","P","ETP","Qobs","Qsim"
        DO i = 2, SIZE(Qan)
            WRITE(iLu,'(I8,4F8.2)') i,Pan(i),ETPan(i),Qan(i),QanSim(i)
        END DO

        ! Fermeture du fichier
        CLOSE(iLu)
    END SUBROUTINE

END MODULE
