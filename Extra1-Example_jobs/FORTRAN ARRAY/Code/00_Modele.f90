MODULE Modele

    CONTAINS

    !**********************************************************************
    SUBROUTINE MOD_GR1A(Param,P0,P1,E1,Q,MISC)
    ! Calculation of streamflow on a single time step (year) with the GR1A model
    ! Inputs
    !       Param ! Vector of real, model parameters (Param(1) [-])
    !       P0    ! Real, value of rainfall during the previous time step [mm/year]
    !       P1    ! Real, value of rainfall during the current time step [mm/year]
    !       E1    ! Real, value of potential evapotranspiration during the current time step [mm/year]
    ! Outputs
    !       Q     ! Real, value of simulated flow at the catchment outlet for the current time step [mm/year]
    !       MISC  ! Vector of real, model outputs for the time step [mm/year]
    !**********************************************************************
          Implicit None

          !! locals
          integer, parameter :: NMISC=3
          integer, parameter :: NParam=2
          real :: tt ! speed-up

          !! dummies
          ! in
          real, dimension(NParam), intent(in) :: Param
          real, intent(in) :: P0,P1,E1
          ! out
          real, dimension(NMISC), intent(out) :: MISC
          real, intent(out) :: Q

    ! NA value
          IF(P0.LT.0. .OR. P1.LT.0. .OR. E1.LE.0.) THEN
            Q = -99
            MISC(:) = -99
          ELSE
      ! Runoff
            tt = (Param(2)*P1+(1-Param(2))*P0)/Param(1)/E1
            Q=P1*(1.-1./SQRT(1.+tt*tt))
      ! Variables storage
            MISC( 1)=E1            ! PE     ! [numeric] observed potential evapotranspiration [mm/year]
            MISC( 2)=P1            ! Precip ! [numeric] observed total precipitation [mm/year]
            MISC( 3)=Q             ! Qsim   ! [numeric] simulated outflow at catchment outlet [mm/year]
          END IF

    END SUBROUTINE

    REAL FUNCTION RMSE(Sim,Obs)

        IMPLICIT NONE

        REAL, DIMENSION(:), INTENT(IN) :: Sim    !< Débits observés
        REAL, DIMENSION(:), INTENT(IN) :: Obs    !< Débits Simulés
        REAL SqError
        INTEGER :: iSize,i


        iSize = 0
        SqError = 0.
        RMSE =  -99
        DO i=1,SIZE(Obs)
            IF(Sim(i).GT.0. .AND. Obs(i).GT.0.) THEN
                SqError = SqError+(Sim(i)-Obs(i))*(Sim(i)-Obs(i))
                iSize=iSize+1
            ENDIF
        END DO
        IF(iSize.GT.0) RMSE =  sqrt(SqError/isize)

    END FUNCTION

END MODULE
