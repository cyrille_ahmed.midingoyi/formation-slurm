# Computing π
The goal here is to compute the number **π** using bash. We will first introduce you to sbatch parameters/options then you will make a batch file to compute PI to the 10000th decimal.

## In case everything goes wrong
You can cancel a job by typing the following command (where JOBID is your unique JOBID) =>
```bash
scancel $JOBID
```

## Sbatch parameters

**IMPORTANT** All parameters must be preceeded by => 
```bash
#SBATCH
```

There is two ways of writing the same options in SBATCH but only the multiple characters one allows you to use every parameters.

### Single character options
The first one is a single letter call to a parameter. You have to keep in mind that a single letter parameter has to be folowed by an empty space then the parameter argument.

### Multiple characters options
With multiple characters options, you have to write them with an "=" at the end of it followed by the parameter argument **no space**.



### Examples
The two parameters are the same =>
```bash
#SBATCH -J PI
#SBATCH --job-name=PI
```

But, this parameter does not exist with a single letter =>
```bash
#SBATCH --ntasks-per-node=1
```


### Quick List of sbatch options =>
```bash
-A or --account=account               # Your group name on the cluster
--begin=date_time                     # If you want to differ the start of the job
-c or --cpus-per-task=N               # How many cores for a single program (OpenMP friendly)
--export=ALL|NONE                     # Identify which environment variables are propagated to the batch job
-J or --job-name=name                 # The name of the job
--mail-type=ALL                       # Warn me by email when ... (ALL is equivalent to BEGIN, END, FAIL, INVALID_DEPEND, REQUEUE, and STAGE_OUT)
--mail-user=user@mail.com             # My email
--mem=1G                              # Amount of memory needed for my job
-N or --nodes=numberofnodes           # The number of nodes requested
-n or --ntasks=number                 # The number of tasks needed
--ntasks-per-node=ntasks              # Request the maximum ntasks be invoked on each node
-o or --output=filename.out           # Specify stdout file
-e or --error=filename.err            # Specify stderr file. By default when "-e" is not specified both the stdout and stderr writes into a single file
-p or --partition=defq                # Specify an initial partition for the job
-t or --time=D-HH:MM:SS               # Set a walltime limit of the job allocation Days, hours, minutes and seconds
```
And many more here [Slurm SBATCH website](https://slurm.schedmd.com/sbatch.html)

Also, [CheatSheet Slurm](https://nextcloud.inrae.fr/s/qs97RCJGF5tkMeK) can be usefull :)

# Exercise 
You have to compute the 10000 decimal of PI using bash.

The following command do just that

```bash
time echo "scale=10000; 4*a(1)" | bc -l
```

Your computer is an old macintosh from 1998 (why not?) and you want to speed things up by using the Meso@LR cluster.

You want to do it right by specifying all the parameters as best as possible which means :
* Name of the job = PI
* Account = groupname (id command results)
* Partition = inrae
* Output and error files redirected to /lustre/$USER/logs (make sure that "logs" folder exist)
* Mail with the status of the job (use your email)
* Memory needed = 1 GB
* Number of cores = 1
* Time Limit = 1 hour
* And then, well... Launch PI

Can you create the right batch file and launch it to compute PI?

The answer is in [pi.sh](./pi.sh) but please try to make it by yourself first :)
