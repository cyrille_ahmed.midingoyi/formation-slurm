# Interactive mode
The interactive mode can be very usefull to:
1. Debug
2. Compile your program
3. Making small ajustments
4. Running small jobs
5. Well, interact with your program if needed (live options)

## srun method
The command "srun --pty /bin/bash" allow you to obtain a shell with default SLURM parameters =>
```bash
[verrierj@muse-login02 ~]$ srun --pty /bin/bash
srun: job 8933557 queued and waiting for resources
srun: job 8933557 has been allocated resources
[verrierj@muse097 ~]$ 
```

You will then be able to interact with the compute node and launch commands to compile, debug... Always use the [SLURM CheatSheet](https://nextcloud.inrae.fr/s/qs97RCJGF5tkMeK) for parameters.

To quit the job, simply type "exit" or "CTRL+c" =>
```bash
(base) [verrierj@muse097 ~]$ ^C
(base) [verrierj@muse097 ~]$ exit
exit
srun: error: muse097: task 0: Exited with exit code 130
(base) [verrierj@muse-login02 ~]$
```

**Warning**, if your connection to the cluster is reseted, lost or if you close your terminal, the session wil be lost forever.

## salloc method
You are first making a reservation for ressources by using the salloc command and then you have to use "srun" as a header to your program in order to use the ressources requested.

For example, with default parameters it can look like this =>
```bash
# Requesting interactive session
[verrierj@muse-login01 ~]$ salloc
salloc: Pending job allocation 8970016
salloc: job 8970016 queued and waiting for resources
salloc: job 8970016 has been allocated resources
salloc: Granted job allocation 8970016

# Printing hostname without srun
[verrierj@muse-login01 ~]$ hostname
muse-login01

# With srun
[verrierj@muse-login01 ~]$ srun hostname
muse106.cluster

# I am running hostname inside the interactive session requested
[verrierj@muse-login01 ~]$ squeue -u verrierj
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           8970016      defq interact verrierj  R       0:11      1 muse106

# quitting interactive session 
[verrierj@muse-login01 ~]$ scancel 8970016
# Exit required for clean shutdown of the session
[verrierj@muse-login01 ~]$ exit
```

## Exercise
In this exercise, you will create an interactive session to compile and test a C code.

### srun method=>

Parameters:
* 8 cores
* 1G of memory
* DEFQ partition
* Time limit of 1 hour

```bash
srun --cpus-per-task 8 --time=01:00:00 --mem=1G -p defq --pty /bin/bash
```

Compile the program using the following commands in the interactive session:

```bash
# Go to scratch directory (You are in a job so $HOME is Read-Only)
cd ~/scratch

# Download the file
wget https://forgemia.inra.fr/formationcalcul2023/formation-slurm/-/raw/main/Exercise7-Interactive_mode/hello_world_omp.c

# Compile it
gcc -fopenmp -o hw_omp hello_world_omp.c

# test it locally
./hw_omp
```
How many tasks do you see?

The output of the code should resemble the following:

```
Hello from thread 0 of 8
Hello from thread 4 of 8
Hello from thread 6 of 8
Hello from thread 7 of 8
Hello from thread 5 of 8
Hello from thread 2 of 8
Hello from thread 1 of 8
Hello from thread 3 of 8
```

### salloc method =>

Exit the interactive srun session and change the parameter "--cpus-per-task" to 12 before opening a new one with salloc and execute the C code:

```bash
# Exiting the srun session
[verrierj@muse104 scratch]$ exit

# Creating a salloc session with 12 cpus
[verrierj@muse-login01 ~]$ salloc --cpus-per-task 12 --time=01:00:00 --mem=1G -p defq
salloc: Pending job allocation 8969934
salloc: job 8969934 queued and waiting for resources
salloc: job 8969934 has been allocated resources
salloc: Granted job allocation 896993

# to interact with my session, I use srun before my program
[verrierj@muse-login01 ~]$ srun scratch/hw_omp
Hello from thread 11 of 12
Hello from thread 9 of 12
Hello from thread 0 of 12
Hello from thread 4 of 12
Hello from thread 8 of 12
Hello from thread 10 of 12
Hello from thread 7 of 12
Hello from thread 2 of 12
Hello from thread 3 of 12
Hello from thread 5 of 12
Hello from thread 6 of 12
Hello from thread 1 of 12

# to close my session, I use scancel 
[verrierj@muse-login01 ~]$ squeue -u $USER
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           8969934      defq interact verrierj  R       5:21      1 muse104
[verrierj@muse-login01 ~]$ scancel 8969934
salloc: Job allocation 8969934 has been revoked.
[verrierj@muse-login01 ~]$ exit
```

If you want to interact with your "salloc" sesion, you wil have to use "srun" before the code you want to run.
How many tasks do you see?

**Do not forget to close your session with scancel!!**

### Additionnal informations
When working with multi-threaded jobs in "sbatch", the variable **OMP_NUM_THREADS** should always ben set to **$SLURM_CPUS_PER_TASK**. 
Reminder, $SLURM_CPUS_PER_TASK is the number of CPUs used for a single program (-c parameter).

