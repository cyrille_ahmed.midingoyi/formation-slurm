# The SLURM VARIABLES
The goal of this section is to make you understand how and why you can use the variables generate by SLURM in your code.

The SLURM documentation is very rich so here is all the informations you will need about this section =>

1. [Filename Pattern](https://slurm.schedmd.com/sbatch.html#SECTION_%3CB%3Efilename-pattern%3C/B%3E)
2. [INPUT ENVIRONMENT VARIABLES](https://slurm.schedmd.com/sbatch.html#SECTION_INPUT-ENVIRONMENT-VARIABLES)
3. [OUTPUT ENVIRONMENT VARIABLES](https://slurm.schedmd.com/sbatch.html#SECTION_OUTPUT-ENVIRONMENT-VARIABLES)

## Exercise 1, filename pattern
Create a simple job that displays the results of the command "hostname" but create all outputs files using =>
```bash
%j    # jobid
%u    # User name
%x    # Job Name
```
**Warning**, these variables are ONLY usable in SLURM parameters starting with #SBATCH! You won't be able to print them with an echo for example or use them in your code.
## Exercise 2, output environment variables
Tune the hostname file you just created to show the following informations in your output file =>
```bash
SLURM_CPUS_PER_TASK    # Number of CPUS per task (openmp)
SLURM_JOB_ID           # JOBID   
SLURM_JOB_ACCOUNT      # ACCOUNT (-A)
SLURM_JOB_NAME         # JOBNAME (-J)
SLURM_NODELIST         # On which nodes the job was computed
SLURM_NTASKS           # How many tasks did it uses?
```