# Connect to Meso@LR
To connect to Meso@LR, you will have to use an ssh client. <br />
The main address is **muse-login.meso.umontpellier.fr**

## Linux and MacOS
If you have Linux or MacOS, you can simply launch a terminal and type =>
```bash
ssh USERNAME@muse-login.meso.umontpellier.fr
```
Where USERNAME is the login name you have been provided by Meso@LR admins.

## Windows
You can use multiple tool to connect to Meso@LR but we strongly suggest you to use [MobaXTerm](https://mobaxterm.mobatek.net/download-home-edition.html).<br />
If you don't have admin rights on your computer, download the "Portable Edition".

Here is how to configure it once installed =>
* Go to "Session"
* Pick "SSH"
* In "Remote Host", type "muse-login.meso.umontpellier.fr"
* Tick "Specify Username" and type the one that Meso@LR sent you
* Click on "Ok"
![MobaXterm Screen Cap](./mobaxterm-screencap.png?raw=true)

You can login to Meso@LR now by double clicking on the session you created in the left panel of MobaXterm.

# Important Notes
## Multiple login nodes
Meso@LR has two login nodes. 
* muse-login01
* muse-login02

They both share the same system but if you tend to use tools like "tmux" or "screen", you should be very careful about where you land when you login.

You can always switch login nodes by doing so =>
```bash
[verrierj@muse-login01 ~]$ ssh muse-login02
verrierj@muse-login02's password:

#######################################################
#                                                     #
#          Bienvenue sur le cluster Muse              #
#                                                     #
#######################################################


[verrierj@muse-login02 ~]$
```